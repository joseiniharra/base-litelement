import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';


class PersonaMain extends LitElement {


    static get properties(){
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Fulano Mengano",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet.",
                photo: {
                    src: "./img/persona1.png",
                    alt: "Fulano Mengano"
                }
            },
            {
                name: "Jerry",
                yearsInCompany: 2,
                profile: "Lorem ipsum dolor sit amet. Amen. Lorem ipsum dolor sit amet. Amen.",
                photo: {
                    src: "./img/persona2.png",
                    alt: "Jerry"
                }
            },
            {
                name: "Gato Tom",
                yearsInCompany: 5,
                profile: "Lorem ipsum dolor sit amet. Gracias.",
                photo: {
                    src: "./img/persona3.png",
                    alt: "Gato Tom"
                }
            },
            {
                name: "Bugs bunny",
                yearsInCompany: 22,
                profile: "Lorem ipsum dolor sit amet. Saludos.",
                photo: {
                    src: "./img/persona4.png",
                    alt: "Jerry"
                }
            },
            {
                name: "Mickye Mouse",
                yearsInCompany: 52,
                profile: "Lorem ipsum dolor sit amet. Hasta luego.",
                photo: {
                    src: "./img/persona5.png",
                    alt: "Jerry"
                }
            }
        ];

        this.showPersonForm = false;

    }

    render(){
       return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person =>  html`<persona-ficha-listado 
                    fname="${person.name}" 
                    yearsInCompany="${person.yearsInCompany}" 
                    profile="${person.profile}" 
                    .photo="${person.photo}"
                    @delete-person="${this.deletePerson}"
                    @info-person="${this.infoPerson}"
                    >
                    </persona-ficha-listado>`
                )}
                </div>
            </div>   
            <div class="row">
                    <persona-form 
                    @persona-form-close="${this.personFormClose}" 
                    @persona-form-store="${this.personFormStore}" 
                    
                    class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
            
       `; 
    }

    updated(changedProperties){
        console.log("updated");

        if (changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            // cambiar valor de class d-none de persona-form

            if (this.showPersonForm === true){
                this.showPersonFormData();
            }
            else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people", {
                "detail": {
                    people: this.people
                }
            }
            
        ));
    }

    }

    

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando listado de personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }


    // borramos personas, con funcion filter cuya salida igualamos a array inicial

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );

    }

    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");


        console.log("La propiedad name en person vale " + e.detail.person.name);
        console.log("La propiedad profile en person vale " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany en person vale " + e.detail.person.yearsInCompany);

        console.log("La propiedad editingPerson vale " + e.detail.editingPerson);

        if (e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);
  
            this.people = this.people.map(
                person => person.name === e.detail.person.name 
                            ? person = e.detail.person : person
            );

            // findIndex devuelve -1 si no ha encontrado nada y el index (>= 0) si lo ha encontrado.

/*            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );
            if (indexOfPerson >=0){
                this.people[indexOfPerson] = e.detail.person;
                console.log("Persona encontrada");
            }
*/            
        } else {
            console.log("Vamos a crear una persona");
            this.people = [...this.people, e.detail.person];

            //            this.people.push(e.detail.person);
        }

        this.showPersonForm = false;

    }

    infoPerson(e){
        console.log("moreInfo");
        console.log("Se ha pedido más información de la persona de nombre " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        console.log(chosenPerson);

        let personToShow = {};
        personToShow.name = chosenPerson[0].name;
        personToShow.profile = chosenPerson[0].profile;
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = personToShow;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
        
    }

}

customElements.define('persona-main', PersonaMain)