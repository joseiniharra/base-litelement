import { LitElement, html } from 'lit-element';

class TestApi extends LitElement {

    static get properties(){
        return {
            movies: {type: Array}
        }
    }

    constructor() {
        super();

        this.movies = [];
        this.getMovieData();
    }

    render(){
       return html`
            ${this.movies.map(
                movie => html`<div>La pelicula ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
       `; 
    }

    getMovieData(){
        console.log("getMovieData");
        console.log("Obteniendo datos de las películas");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            // Estamos contemplando el 200, pero estoy hay que consesuarlo con el BACK
            if (xhr.status === 200){
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                //console.log(APIResponse);
                // Esto va a cambiar en Hackathon
                this.movies = APIResponse.results;
            }
        };
        // Aquí pondremos la url que nos digan los del BACK
        xhr.open("GET", "https://swapi.dev/api/films");
        xhr.send();
        console.log("Fin de getMovieData");

    }

}

customElements.define('test-api', TestApi)